module 01-Authorization-RS256-BETA

go 1.16

require (
	github.com/auth0/go-jwt-middleware/v2 v2.0.0-beta
	github.com/joho/godotenv v1.4.0
)
